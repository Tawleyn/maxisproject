using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;
using System.Linq;

using TMPro;

namespace MaxisProject
{
    public class Main : MonoBehaviour
    {
        int[] birthDates;
        int[] deathDates;
        string[] allLines;
        string[] lineSubset;

        MergeSort mergeSort = new MergeSort();

        static string filePath = "Assets/dataset-BestTimeToBeAlive.txt";

        // UI Setup.
        public GameObject contentRect, template;
        public List<TMP_Text> info;
        public List<GameObject> objectsInRect;

        private void Start()
        {
            // Initialize the line subset for randomly picking lines from the file.
            allLines = File.ReadAllLines(filePath);
            lineSubset = allLines;
        }

        public void MainButton()
        {
            // Execution time tracking.
            DateTime startTime = DateTime.Now;

            int currentNumberOfAliveComposers = 0;
            int highestNumberOfAliveComposers = 0;
            int currentSpan = 0;
            int highestSpan = 0;
            int currentSpanStartYear = 0;
            int currentSpanEndYear;

            int numberOfLines = lineSubset.Length;
            birthDates = new int[numberOfLines];
            deathDates = new int[numberOfLines];

            int[] bestYears = new int[2];

            // Separate the input lines into two int arrays storing birth dates and death dates.
            for (int i = 0; i < numberOfLines; i++)
            {
                // Because of consistent formatting, we can remove all data after the dates, and the opening bracket.
                string dates = lineSubset[i].Remove(10).Remove(0, 1);

                // Similarly, we know birth and death dates are separated by a '-' symbol.
                int birth = int.Parse(dates.Split('-')[0]);
                int.TryParse(dates.Split('-')[1], out int death);

                if (death == 0 || death > 1993)
                {
                    death = 1993;
                }

                birthDates[i] = birth;
                deathDates[i] = death;
            }

            // For now, the composer names don't matter, so we can sort the dates themselves in ascending order.
            mergeSort.Sort(birthDates, 0, numberOfLines - 1);
            mergeSort.Sort(deathDates, 0, numberOfLines - 1);

            int birthIndex = 0;
            int deathIndex = 0;
            bool isBestTime = true;

            // It can be assumed that this for loop will never grow to unmanageable sizes unless our scale for time increases dramatically.
            for(int currentYear = birthDates[0]; currentYear <= 1993; currentYear++)
            {
                // Because it is assumed that composers are alive for the entire year of their birth, we can update births at the start of the loop. Make sure we haven't run out of births first.
                while (birthIndex < birthDates.Length && birthDates[birthIndex] == currentYear)
                {
                    currentNumberOfAliveComposers++;
                    birthIndex++;
                }

                // Once all the births have been processed for the current year, check if the number of composers alive has increased past the highest recorded alive.
                if (currentNumberOfAliveComposers > highestNumberOfAliveComposers)
                {
                    highestNumberOfAliveComposers = currentNumberOfAliveComposers;

                    // We are in a new span for the best time to be alive.
                    currentSpanStartYear = currentYear;
                    isBestTime = true;
                    currentSpan = 1;
                    highestSpan = 1;
                } 
                else if (currentNumberOfAliveComposers < highestNumberOfAliveComposers && isBestTime)
                {
                    // If we are currently in the best time to be alive, and the number of composers drops below the highest number, it is the end of the best time.
                    currentSpanEndYear = currentYear;
                    isBestTime = false;

                    // We check if the current span is higher than the highest span and set if true.
                    if(currentSpan > highestSpan)
                    {
                        highestSpan = currentSpan;

                        // Additionally, since this is now the highest span, we can record the best start and end years.
                        bestYears[0] = currentSpanStartYear;
                        bestYears[1] = currentSpanEndYear;
                    }
                }
                else if(currentNumberOfAliveComposers == highestNumberOfAliveComposers)
                {
                    // If we are already in the best time, increment the span.
                    if (isBestTime)
                    {
                        currentSpan++;
                    }
                    else
                    {
                        // If we are not in the best time, we have entered a new span with alive composers equal to the previous span.
                        currentSpanStartYear = currentYear;
                        currentSpan = 1;
                        isBestTime = true;
                    }
                }

                // Because it is assumed that composers are alive for the entire year of their death, we should update deaths at the end of the loop.
                while(deathIndex < deathDates.Length && deathDates[deathIndex] == currentYear)
                {
                    currentNumberOfAliveComposers--;
                    deathIndex++;
                }
            }

            // Tracking the execution time, and other useful info for display.
            double elapsedTime = (DateTime.Now - startTime).TotalMilliseconds;
            PopulateExecutionInfo(highestNumberOfAliveComposers, bestYears, elapsedTime);
        }        
        
        // Generate UI objects to fill the scroll rect.
        public void PopulateScrollRect()
        {
            // Clear the rect of existing objects.
            foreach(GameObject item in objectsInRect)
            {
                Destroy(item);
            }
            objectsInRect.Clear();

            // Use a template prefab to populate the scrollable list.
            foreach (string line in lineSubset)
            { 
                GameObject newObject = Instantiate(template);
                newObject.transform.SetParent(contentRect.transform);
                newObject.transform.localScale = new Vector3(1, 1, 1);
                newObject.GetComponent<TMP_Text>().text = line;
                objectsInRect.Add(newObject);
            }
        }

        // Generate useful info for display.
        public void PopulateExecutionInfo(int alive, int[] dates, double executionTime)
        {
            info[0].text = alive.ToString();
            info[1].text = dates[0].ToString() + "-" + dates[1].ToString();
            info[2].text = executionTime.ToString() + "ms";
        }

        // Select a random number of lines inside the sample file to test based on user input inside the editor. Using -1 will generate the entire sample file.
        public void NewLineSubset(int numberOfLines)
        {
            if (numberOfLines != -1)
            {
                // Generate an array of user defined length of random numbers from 0 to the number of lines in the sample file.
                System.Random rng = new System.Random();
                int[] newList = Enumerable.Range(0, allLines.Length).OrderBy(x => rng.Next()).Take(numberOfLines).ToArray();

                // Reset the line subset and populate it with the lines from the sample file based on the random numbers.
                lineSubset = new string[numberOfLines];
                for (int i = 0; i < numberOfLines; i++)
                {
                    lineSubset[i] = allLines[newList[i]];
                }
            }
            PopulateScrollRect();
        }
    }
}