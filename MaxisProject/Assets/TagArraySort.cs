using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MaxisProject
{
    // To use:
    // Create a tag array equal to the length of the 2-dimensional array ->     int[] tagArray = new int[numberOfElementsInFirstColumn];
    // Initialize the IComparer and sort ->                                     TagArraySort mySorter = new TagArraySort(2DArrayToBeSorted);
    //                                                                          Array.Sort(tagArray, mySorter);

    // Utilizing IComparer and the built-in sort function for arrays, the tag array now represents a sorted version of the original 2D array with 
    // each element in the tag array representing the index of each element in the original array in their sorted order.
    public class TagArraySort : IComparer
    {
        // A reference to the original 2-dimensional array
        private int[,] sortArray;

        // Initialization
        public TagArraySort(int[,] array)
        {
            sortArray = array;
        }

        // Implementation of the IComparer function.
        public int Compare(object x, object y)
        {
            // Integer row numbers of the sortArray
            int i1 = (int)x;
            int i2 = (int)y;

            // Use built in CompareTo
            return sortArray[i1, 0].CompareTo(sortArray[i2, 0]);
        }
    }
}
