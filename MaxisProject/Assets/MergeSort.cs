using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MaxisProject
{
    public class MergeSort : MonoBehaviour
    {
        // Sort an array by splitting it into two halves of the original.
        public int[] Sort(int[] inputArray, int leftIndex, int rightIndex)
        {
            if (leftIndex < rightIndex)
            {
                int middleIndex = leftIndex + (rightIndex - leftIndex) / 2;

                // Continue to recursively split the two new arrays into smaller arrays.
                Sort(inputArray, leftIndex, middleIndex);
                Sort(inputArray, middleIndex + 1, rightIndex);

                // Once the recursion cannot split the array further, begin merging the arrays.
                Merge(inputArray, leftIndex, middleIndex, rightIndex);
            }

            return inputArray;
        }

        // Merge an array based on the required indices.
        public void Merge(int[] array, int leftIndex, int middleIndex, int rightIndex)
        {
            int leftArrayLength = middleIndex - leftIndex + 1;
            int rightArrayLength = rightIndex - middleIndex;
            int[] leftTempArray = new int[leftArrayLength];
            int[] rightTempArray = new int[rightArrayLength];
            int i, j;

            // Setup the left and right arrays.
            for (i = 0; i < leftArrayLength; ++i)
                leftTempArray[i] = array[leftIndex + i];
            for (j = 0; j < rightArrayLength; ++j)
                rightTempArray[j] = array[middleIndex + 1 + j];

            i = 0;
            j = 0;
            int k = leftIndex;

            // Merge the two arrays, alternating new elements between the smaller value of their respective indices.
            while (i < leftArrayLength && j < rightArrayLength)
            {
                if (leftTempArray[i] <= rightTempArray[j])
                {
                    array[k++] = leftTempArray[i++];
                }
                else
                {
                    array[k++] = rightTempArray[j++];
                }
            }

            // If we have uneven arrays and there are leftover values from one array, append them to the newly merged array.
            while (i < leftArrayLength)
            {
                array[k++] = leftTempArray[i++];
            }

            while (j < rightArrayLength)
            {
                array[k++] = rightTempArray[j++];
            }
        }
    }
}
